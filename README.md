# Skype Database Tool #

SkypeDBTool is a command-line utility to view Skype database and export messages into a text file in readable format.

The latest binaries can be found on [Downloads](https://bitbucket.org/Cr7pto/skype-db-tool/downloads) page.

### Command-line syntax ###

`SkypeDBTool.Cmd.exe -s <path> -o <CONV|MSG> [-c <id>] [-l <limit>] [-y <year>|<yearFrom-yearTo>] [-a <*|path>] [-p] [-f <CHATLOG|XML|TEXTONLY>]`


`-s`, `--source`	Required. Skype main.db file (or path) to be processed.

`-o`, `--operation`	Required. Defines the operation. CONV - to list conversations, MSG - to list messages.

`-c`, `--conv-id`	Defines the target conversation Id.

`-l`, `--limit`	Specifies the number of recent items to output.

`-y`, `--year`	Specifies the year (or year interval) to filter messages.

`-a`, `--aliases`	Specifies to use aliases used instead of author names. Specify '-a *' to use actual names of chat members; or specify the path to the UTF8 text file with author aliases containing one alias per line in a format 'identity|alias'

`-p`, `--paging`	Makes a pause after filling the screen.

`-f`, `--format`	Defines the format used to output messages (CHATLOG, XML, TEXTONLY). Chat Log format is used by default if format is not specified explicitly.

`-m`, `--merged-sources`	Defines the additional sources files (either Skype main.db or XML files with messages). Result messages are merged from all sources. It's useful to merge chat logs from separate Skype instances.

`--help`	Display the help screen.


### Examples ###

- Displays 20 recent conversations:

`SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o conv -l 20`

- Displays the information of the specified conversation with Id = 1234:

`SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o conv -c 1234`

- Displays 100 last messages from the specified conversation with Id = 1234 using actual author names:

`SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o msg -c 1234 -l 100 -a * -p`

- Exports all messages from the specified conversation with Id = 1234 by the year 2015 to output.txt using aliases.txt:

`SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o msg -c 1234 -y 2015 -a aliases.txt >> output.txt`

- Exports all messages from the specified conversation with Id = 1234 merged with messages from the corresponding conversation in another Skype db to output.txt using aliases.txt:

`SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o msg -c 1234 -a aliases.txt -m %AnotherSkypeDBPath% >> output.txt`