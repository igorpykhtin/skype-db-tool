@echo off 
setlocal 

set SolutionPath=src\SkypeDBTool.sln
set ProjectOutputPath=src\SkypeDBTool.Cmd\bin\Release
set PublishPath=bin

:: Build
echo Building...
"%programfiles(x86)%\MSBuild\14.0\Bin\msbuild.exe" "%SolutionPath%" /t:Build /p:Configuration=Release /p:Platform="Any CPU"

set BuildErrorLevel=%ErrorLevel%
if ErrorLevel 1 goto End

:AfterBuild 

echo. 

:: Publish
echo Publishing...
xcopy "%ProjectOutputPath%\*.*" "%PublishPath%\" /EXCLUDE:build\deploy_exclude.txt /E /Y

set BuildErrorLevel=%ErrorLevel%
if ErrorLevel 1 goto End

:AfterPublish

echo. 

:End

echo Build Exit Code = %BuildErrorLevel% 
pause

exit /b %BUILDERRORLEVEL%