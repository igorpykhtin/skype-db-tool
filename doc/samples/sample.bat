@echo off
rem change CHCP to UTF-8
chcp 65001
rem cls

set ToolPath=bin
set SkypeProfilePath=%appdata%\Skype\some_skype_user\
set ConversationId=123
set AliasesFileName=aliases.txt
set AdditionalSkypeDB="D:\Temp\SkypeFromOtherPC\"

mkdir output

del output\Conversations_Recent.txt
%ToolPath%\SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o conv -l 20 >> output\Conversations_Recent.txt

del output\Conversations.txt
%ToolPath%\SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o conv >> output\Conversations.txt

del output\Chat_Info.txt
%ToolPath%\SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o conv -c %ConversationId%  >> output\Chat_Info.txt

del output\Chat_Last100.txt
%ToolPath%\SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o msg -c %ConversationId% -a %AliasesFileName% -l 100 >> output\Chat_Last100.txt

del output\Chat.txt
%ToolPath%\SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o msg -c %ConversationId% -a %AliasesFileName% >> output\Chat.txt

del output\Chat.xml
%ToolPath%\SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o msg -c %ConversationId% -f xml >> output\Chat.xml

del output\Chat_TextOnly_2015.txt
%ToolPath%\SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o msg -c %ConversationId% -f textonly -y 2015 >> output\Chat_TextOnly_2015.txt

del output\Chat_Merged.txt
%ToolPath%\SkypeDBTool.Cmd.exe -s %SkypeProfilePath% -o msg -c %ConversationId% -a %AliasesFileName% -m %AdditionalSkypeDB% >> output\Chat_Merged.txt

