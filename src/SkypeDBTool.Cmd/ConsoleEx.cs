﻿using System;
using System.Collections.Generic;
using SkypeDBTool.Core.Formatters;

namespace SkypeDBTool.Cmd
{
	static class ConsoleEx
	{
		public static bool Paging { get; set; }

		public static void Print<T>(IEnumerable<T> items, Action<T> printItem, string separatorLine, bool printTotal)
		{
			int i = 0;
			foreach (var item in items)
			{
				i++;
				printItem(item);

				PageBreak(i);
			}

			if (printTotal)
			{
				Console.WriteLine(separatorLine);
				Console.WriteLine($"Total: {i}");
			}
		}

		public static void Print<T>(IEnumerable<T> items, IEntityWriter<T> writer)
		{
			writer.Begin(Console.Out);

			Print(items, item => writer.Write(item, Console.Out), default(string), false);

			writer.End(Console.Out);
		}

		private static void PageBreak(int iteration)
		{
			if (!Paging)
			{
				return;
			}

			bool screenFilled = (iteration % (Console.WindowHeight - 2) == 0);

			if (!screenFilled)
			{
				return;
			}

			Console.Write("Press any other key to continue (Esc - turn off paging, Ctrl+C - exit)...");
			var key = Console.ReadKey(intercept: true);

			Console.SetCursorPosition(0, Console.CursorTop);
			ClearLine();

			if (key.Key == ConsoleKey.Escape)
			{
				Paging = false;
			}
		}

		/// <summary>
		/// Clears current line in the console window.
		/// </summary>
		public static void ClearLine()
		{
			int currentLineCursor = Console.CursorTop;
			Console.SetCursorPosition(0, Console.CursorTop);
			Console.Write(new string(' ', Console.WindowWidth));
			Console.SetCursorPosition(0, currentLineCursor);
		}

		public static void PrintError(Exception ex, bool fullInfo = false)
		{
			ConsoleColor previousColor = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine(fullInfo ? ex.ToString() : ex.Message);
			Console.ForegroundColor = previousColor;
		}
	}
}
