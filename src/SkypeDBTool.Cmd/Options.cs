﻿using System;
using System.ComponentModel;
using CommandLine;
using CommandLine.Text;
using SkypeDBTool.Core;

namespace SkypeDBTool.Cmd
{
	class Options
	{
		[Option('s', "source", Required = true, HelpText = @"Skype main.db file (or path) to be processed. Example: -s %appdata%\Skype\SomeUser")]
		public string Source { get; set; }

		[Option('o', "operation", Required = true, HelpText = "Defines the operation. CONV - to list conversations, MSG - to list messages. Example: -o CONV or -o MSG")]
		public Operations Operation { get; set; }

		[Option('c', "conv-id", Required = false, HelpText = "Defines the target conversation Id. Example: -c 1234")]
		public int? ConversationId { get; set; }

		[Option('l', "limit", Required = false, HelpText = "Specifies the number of recent items to output. Example: -l 10")]
		public int? Limit { get; set; }

		[Option('y', "year", Required = false, HelpText = "Specifies the year (or year interval) to filter messages . Example: -y 2015 or -y 2011-2014")]
		public string YearFilter { get; set; }

		[Option('a', "aliases", Required = false, HelpText = "Specifies to use aliases used instead of author names. Specify '-a *' to use actual names of chat members; or specify the path to the UTF8 text file with author aliases containing one alias per line in a format 'identity|alias'. Example: -a * or -a aliases.txt")]
		public string Aliases { get; set; }

		[Option('p', "paging", Required = false, HelpText = "Makes a pause after filling the screen. Example: -p")]
		[DefaultValue(false)]
		public bool Paging { get; set; }

		[Option('f', "format", Required = false, HelpText = "Defines the format used to output messages (CHATLOG, XML, TEXTONLY). Chat Log format is used by default if format is not specified explicitly. Example: -f XML or -f TEXTONLY")]
		public Formats Format { get; set; }

		// TODO: probably get rid of this and use Source as array instead
		[OptionArray('m', "merged-sources", Required = false, HelpText = "Defines the additional sources files (either Skype main.db or XML files with messages). Result messages are merged from all sources. It's useful to merge chat logs from separate Skypes. Example: -m  D:\\Temp\\SecondSkypeSource\\ D:\\Temp\\ThirsdSkypeSource\\main.db D:\\Temp\\FourthSkypeSource\\messages.xml")]
		public string[] MergedSources { get; set; }

		[HelpOption]
		public string GetUsage()
		{
			return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
		}

		public enum Operations
		{
			CONV,
			MSG
		}

		public enum Formats
		{
			CHATLOG,
			XML,
			TEXTONLY
		}

		public DateTimeInterval DateFilter
		{
			get
			{
				DateTimeInterval result;
				int year;
				if (String.IsNullOrWhiteSpace(YearFilter))
				{
					result = DateTimeInterval.Infinity;
				}
				else if (YearFilter.Contains("-"))
				{
					var parts = YearFilter.Split('-');
					int yearFrom = Convert.ToInt32(parts[0]);
					int yearTo = Convert.ToInt32(parts[1]);
					result =  DateTimeInterval.FromYearInterval(yearFrom, yearTo);
				}
				else if (Int32.TryParse(YearFilter, out year))
				{
					result = DateTimeInterval.FromYear(year);
				}
				else
				{
					throw new ArgumentException($"Year filter has invalid format");
				}

				return result;
			}
		}

		private DateTime FromYear(int year) => new DateTime(year, 1, 1);
	}
}
