﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using SkypeDBTool.Core;
using SkypeDBTool.Core.Entity;
using SkypeDBTool.Core.Formatters;

namespace SkypeDBTool.Cmd
{
	class Program
	{
		static void Main(string[] args)
		{
			var options = new Options();
			if (!CommandLine.Parser.Default.ParseArguments(args, options))
			{
				Environment.Exit(CommandLine.Parser.DefaultExitCodeFail);
			}

			try
			{
				Process(options);
			}
			catch (ArgumentException ex)
			{
				ConsoleEx.PrintError(ex, fullInfo: false);
				Environment.ExitCode = 1;
			}
			catch (NotSupportedException ex)
			{
				ConsoleEx.PrintError(ex, fullInfo: false);
				Environment.ExitCode = 1;
			}
			catch (Exception ex)
			{
				ConsoleEx.PrintError(ex, fullInfo: true);
				Environment.ExitCode = 1;
			}
		}

		private static void Process(Options options)
		{
			ConsoleEx.Paging = options.Paging;

			if (options.Operation == Options.Operations.CONV)
			{
				ProcessConversations(options);
			}
			else if (options.Operation == Options.Operations.MSG)
			{
				ProcessMessages(options);
			}
		}

		private static void ProcessConversations(Options options)
		{
			var skype = CreateSkype(options.Source);

			if (options.ConversationId.HasValue)
			{
				var conversation = skype.GetConversation(options.ConversationId.Value);

				if (conversation == null)
				{
					throw new ArgumentException($"No conversation found with Id = {options.ConversationId}");
				}

				Print(conversation);
			}
			else
			{
				var conversations = options.Limit.HasValue
					? skype.GetRecentConversations(options.Limit.Value)
					: skype.GetConversations();

				Print(conversations);
			}
		}

		private static void ProcessMessages(Options options)
		{
			if (!options.ConversationId.HasValue)
			{
				throw new ArgumentNullException("Conversation Id is required", nameof(options.ConversationId));
			}

			var skype = CreateSkype(options.Source);

			var conversation = skype.GetConversation(options.ConversationId.Value);

			if (conversation == null)
			{
				throw new ArgumentException($"No conversation found with Id = {options.ConversationId}");
			}

			IEnumerable<Message> messages;
			if (options.MergedSources == null || options.MergedSources.Length == 0)
			{
				messages = GetMessagesFromSource(conversation.Conversation.Id, options, skype);
			}
			else
			{
				if (options.Limit.HasValue)
				{
					throw new NotSupportedException("Limit not supported in merge mode");
				}

				var messageSources = new List<IEnumerable<Message>>();

				// original source
				messageSources.Add(GetMessagesFromSource(conversation.Conversation.Id, options, skype));

				// additional sources
				foreach (var source in options.MergedSources)
				{
					IEnumerable<Message> messageSource;

					if (Path.GetExtension(source).ToLower() == ".xml")
					{
						messageSource = GetMessagesFromXml(source, options);
					}
					else
					{
						var skype2 = CreateSkype(source);

						var conversation2 = skype2.FindConversation(conversation.Conversation.Identity);
						if (conversation2 == null)
						{
							throw new ArgumentException($"No conversation found with Identity = '{conversation.Conversation.Identity}' in additional source DB '{source}'");
						}

						messageSource = GetMessagesFromSource(conversation2.Id, options, skype2);
					}

					messageSources.Add(messageSource);
				}

				var merger = new MessageMerger();
				messages = merger.Merge(messageSources.ToArray());
			}

			var authorNameStrategy = CreateAuthorNameStrategy(options, skype);

			Print(messages, options, authorNameStrategy);
		}

		private static IEnumerable<Message> GetMessagesFromSource(int conversationId, Options options, ISkypeRepository skype)
		{
			return options.Limit.HasValue
				? skype.GetLastMessages(conversationId, options.Limit.Value, options.DateFilter)
				: skype.GetMessages(conversationId, options.DateFilter);
		}

		private static IEnumerable<Message> GetMessagesFromXml(string fileName, Options options)
		{
			using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
			using (var reader = new StreamReader(stream))
			{
				var messages = MessageXmlSource.GetMessages(reader, options.DateFilter);

				foreach (var message in messages)
				{
					yield return message;
				}
			}
		}

		private static ISkypeRepository CreateSkype(string path)
		{
			return new SkypeDb(path);
		}

		private static IAuthorNameStrategy CreateAuthorNameStrategy(Options options, ISkypeRepository skype)
		{
			IAuthorNameStrategy authorNameStrategy;

			if (options.Aliases == "*")
			{
				authorNameStrategy = new ActualAuthorNameStrategy(skype);
			}
			else if (options.Aliases != null)
			{
				var lines = File.ReadLines(options.Aliases);
				authorNameStrategy = new AliasAuthorNameStrategy(lines);
			}
			else
			{
				authorNameStrategy = new DefaultAuthorNameStrategy();
			}

			return authorNameStrategy;
		}

		private static void Print(IEnumerable<Conversation> conversations)
		{
			const string tableLine = "----------------+------------------------------------------------";

			Console.WriteLine("Conversation Id | Display Name");
			Console.WriteLine(tableLine);

			ConsoleEx.Print(
				conversations,
				conversation => Console.WriteLine($"{conversation.Id,15} | {conversation.DisplayName}"),
				tableLine,
				printTotal: true
				);
		}

		private static void Print(ConversationInfo conversation)
		{
			const int columnWidth = 25;

			Console.WriteLine($"{("Conversation Id"),-columnWidth} : {conversation.Conversation.Id}");
			Console.WriteLine($"{("Display Name"),-columnWidth} : {conversation.Conversation.DisplayName}");
			Console.WriteLine($"{("Conversation Identity"),-columnWidth} : {conversation.Conversation.Identity}");
			Console.WriteLine($"{("Number of Messages"),-columnWidth} : {conversation.NumberOfMessages}");
			Console.WriteLine($"{("Creation Date"),-columnWidth} : {conversation.Conversation.CreateDate.ToLocalTime()}");
			Console.WriteLine($"{("Last Activity"),-columnWidth} : {conversation.Conversation.LastActivityDate?.ToLocalTime()}");
			Console.WriteLine("Members:");
			const string tableLine = "--------------------------+------------------------------------------------";

			Console.WriteLine(tableLine);
			Console.WriteLine($"{("Identity"),columnWidth} | Display Name(s)");
			Console.WriteLine(tableLine);

			ConsoleEx.Print(
				conversation.Authors,
				author => Console.WriteLine($" {author.Identity,-(columnWidth-1)} | {String.Join(", ", author.DisplayNames)}"),
                tableLine,
				printTotal: true
				);
		}

		private static void Print(IEnumerable<Message> messages, Options options, IAuthorNameStrategy authorNameStrategy)
		{
			var writer = CreateMessageWriter(options, authorNameStrategy);

			ConsoleEx.Print(messages, writer);
		}

		private static IEntityWriter<Message> CreateMessageWriter(Options options, IAuthorNameStrategy authorNameStrategy)
		{
			IEntityWriter<Message> formatter;
			if (options.Format == Options.Formats.TEXTONLY)
			{
				formatter = new TextOnlyMessageFormatter();
			}
			else if (options.Format == Options.Formats.XML)
			{
				formatter = new XmlMessageFormatter();
			}
			else
			{
				formatter = new ChatLogMessageFormatter(authorNameStrategy);
			}

			return formatter;
		}
	}
}
