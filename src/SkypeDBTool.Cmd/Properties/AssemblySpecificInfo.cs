﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Skype DB Tool")]
[assembly: AssemblyDescription("A command-line utility to view Skype database.")]

[assembly: Guid("bc3f9ba9-5a23-4d79-a405-8eb2493a11cd")]