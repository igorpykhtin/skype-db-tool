﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core
{
	public interface IAuthorNameStrategy
	{
		string GetAuthorName(Message message);
	}

	public class DefaultAuthorNameStrategy : IAuthorNameStrategy
	{
		public string GetAuthorName(Message message)
		{
			return message.AuthorDisplayName;
		}
	}

	public class ActualAuthorNameStrategy : IAuthorNameStrategy
	{
		private readonly ISkypeRepository _skype;
		private readonly ConcurrentDictionary<string, string> _aliasCache = new ConcurrentDictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

		public ActualAuthorNameStrategy(ISkypeRepository skype)
		{
			if (skype == null)
			{
				throw new ArgumentNullException(nameof(skype));
			}

			_skype = skype;
		}


		public string GetAuthorName(Message message)
		{
			var result = _aliasCache.GetOrAdd(message.Author, _skype.GetActualAuthorName);

			Debug.Assert(result != null);

			return result;
		}
	}

	public class AliasAuthorNameStrategy : IAuthorNameStrategy
	{
		private readonly IDictionary<string, string> _aliasCache = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

		public AliasAuthorNameStrategy(IEnumerable<string> lines)
		{
			if (lines == null)
			{
				throw new ArgumentNullException(nameof(lines));
			}

			InitializeCache(lines.Where(o => !String.IsNullOrWhiteSpace(o)));
		}

		private void InitializeCache(IEnumerable<string> lines)
		{
			foreach (string line in lines)
			{
				var parts = line.Split('|');

				if (parts.Length != 2)
				{
					throw new ArgumentException($"Invalid alias file format. Line: {line}");
				}

				string identity = parts[0];
				string alias = parts[1];

				if (String.IsNullOrWhiteSpace(identity)
					|| String.IsNullOrWhiteSpace(alias))
				{
					throw new ArgumentException($"Invalid alias file format. Line: {line}");
				}

				string value;
				if (!_aliasCache.TryGetValue(identity, out value))
				{
					_aliasCache[identity] = alias;
				}
				else
				{
					throw new ArgumentException($"Invalid alias file format. Duplicate name {parts[0]}.");
				}
			}
		}


		public string GetAuthorName(Message message)
		{
			string result;

			return _aliasCache.TryGetValue(message.Author, out result) ? result : message.AuthorDisplayName;
		}
	}
}
