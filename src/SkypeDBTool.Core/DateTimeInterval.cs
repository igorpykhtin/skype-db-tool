﻿using System;

namespace SkypeDBTool.Core
{
	public struct DateTimeInterval
	{
		public static readonly DateTimeInterval Infinity = new DateTimeInterval();

		/// <summary>
		/// Start date (inclusive).
		/// </summary>
		public DateTime? From { get; }

		/// <summary>
		/// End date (not inclusive).
		/// </summary>
		public DateTime? To { get; }

		public DateTimeInterval(DateTime? from, DateTime? to)
		{
			From = from;
			To = to;
		}

		public static DateTimeInterval FromYear(int year) => new DateTimeInterval(new DateTime(year, 1, 1), new DateTime(year + 1, 1, 1));
		public static DateTimeInterval FromYearInterval(int from, int to) => new DateTimeInterval(new DateTime(from, 1, 1), new DateTime(to + 1, 1, 1));
	}
}
