﻿using System;

namespace SkypeDBTool.Core
{
	public static class DateTimeUnix
	{
		public static readonly DateTime BaseDate = DateTimeOffset.FromUnixTimeSeconds(0).UtcDateTime;

		public static long To(DateTime dateTime)
		{
			return new DateTimeOffset(dateTime.ToUniversalTime()).ToUnixTimeSeconds();
		}

		public static DateTime From(long timestamp)
		{
			return DateTimeOffset.FromUnixTimeSeconds(timestamp).UtcDateTime;
		}

		public static long? To(DateTime? dateTime)
		{
			return dateTime.HasValue ? To(dateTime.Value) : default(long?);
		}

		public static DateTime? From(long? timestamp)
		{
			return timestamp.HasValue ? From(timestamp.Value) : default(DateTime);
		}
	}
}
