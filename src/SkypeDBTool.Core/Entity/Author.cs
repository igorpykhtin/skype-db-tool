﻿using System.Collections.Generic;

namespace SkypeDBTool.Core.Entity
{
	public class Author
	{
		public string Identity { get; set; }

		public List<string> DisplayNames { get; } = new List<string>();
	}

	internal class AuthorPair
	{
		public string Identity { get; set; }

		public string DisplayName { get; set; }
	}
}
