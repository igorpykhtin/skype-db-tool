﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkypeDBTool.Core.Entity
{
	[Table("Conversations")]
	public class Conversation
	{
		[Column("id")]
		public int Id { get; set; }
		
		[Column("identity")]
		public string Identity { get; set; }

		[Column("displayname")]
		public string DisplayName { get; set; }

		[Column("creation_timestamp")]
		public DateTime CreateDate { get; set; }

		[Column("last_message_id")]
		public int? LastMessageId { get; set; }

		[Column("last_activity_timestamp")]
		public DateTime? LastActivityDate { get; set; }
	}
}
