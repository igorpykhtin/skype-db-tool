﻿using System.Collections.Generic;

namespace SkypeDBTool.Core.Entity
{
	public class ConversationInfo
	{
		public Conversation Conversation { get; set; }

		public int? NumberOfMessages{ get; set; }

		public List<Author> Authors { get; } = new List<Author>();
	}
}
