﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Xml;
using System.Xml.Serialization;

namespace SkypeDBTool.Core.Entity
{
	partial class Message
	{
		[NotMapped]
		[XmlElement("body_xml", Order = 8)]
		public XmlCDataSection _BodyXmlCData
		{
			get
			{
				if (BodyXml == null)
				{
					return null;
				}

				string text = SerializationUtil.EscapeInvalidXmlChar(BodyXml);

				var doc = new XmlDocument();
				return doc.CreateCDataSection(text);
			}
			set
			{
				BodyXml = value != null ? value.Value : null;
			}
		}

		[NotMapped]
		[XmlElement("guid", Order = 3)]
		public string _Guid
		{
			get
			{
				return SkypeUtil.GuidToString(this.Guid);
			}
			set
			{
				this.Guid = value != null ? SkypeUtil.StringToGuid(value) : null;
			}
		}
	}
}
