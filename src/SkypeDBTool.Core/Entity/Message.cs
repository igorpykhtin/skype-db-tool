﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace SkypeDBTool.Core.Entity
{
	[Table("Messages")]
	[XmlType("message")]
	public partial class Message
	{
		[Column("id")]
		[XmlElement("id", Order = 1)]
		public int Id { get; set; }

		[Column("convo_id")]
		[XmlElement("convo_id", Order = 2)]
		public int ConversationId { get; set; }

		/// <summary>
		/// Represents a 32-bytes value that unique identifies the message worldwide.
		/// </summary>
		[Column("guid")]
		[XmlIgnore]
		public byte[] Guid { get; set; }

		[Column("timestamp")]
		[XmlElement("timestamp", Order = 4)]
		public DateTime Date { get; set; }

		[Column("author")]
		[XmlElement("author", Order = 5)]
		public string Author { get; set; }

		[Column("from_dispname")]
		[XmlElement("from_dispname", Order = 6)]
		public string AuthorDisplayName { get; set; }

		[Column("type")]
		[XmlElement("type", Order = 7)]
		public int? MessageType { get; set; }

		[Column("body_xml")]
		[XmlIgnore]
		public string BodyXml { get; set; }

		[Column("identities")]
		[XmlElement("identities", Order = 9)]
		public string Identities { get; set; }
	}
}
