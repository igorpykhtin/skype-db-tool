﻿namespace SkypeDBTool.Core.Entity
{
	public enum MessageType
	{
		ChangeChatInfo = 2,
		// 4?
		// 8?
		AddChatMember = 10, // identities - target members
		KickChatMember = 12, // identities - target members
		LeaveChat = 13,
		Call = 30, // <partlist ... identities - participants
		CallAccept = 39, // <partlist ...
		InviteMessage = 50, // identities - to whom the invite has been sent
		AddContact = 51, // identities - target contact
		DeclineContact = 53, // identities - target contact
		Status = 60, // /me ...
		Message = 61,
		Contacts = 63, // <contacts alt="[Contacts enclosed. Please upgrade to latest Skype version to receive contacts.]"><c t="s" s="k-real777" f="Kirill Alexandrovich"/></contacts>
		Sms = 64, // <sms alt="Это смс-ка, спасающая от деактивации!"><type>2</type><status>4</status><failurereason>0</failurereason><targets><target status="3">+79166389382</target></targets><price>0.053 EUR</price><body><chunk id="0">Это смс-ка, спасающая от деактивации!</chunk><untilnext>33</untilnext></body><encoded_body>Это смс-ка, спасающая от деактивации!</encoded_body><sendtimestamp>1419260304</sendtimestamp></sms>
		File = 68, // <files ...
		NewChat = 100, // identities - participants
		// 110?
		Image = 201, // <URIObject ...
		Clip = 253, // <URIObject ...
	}
}
