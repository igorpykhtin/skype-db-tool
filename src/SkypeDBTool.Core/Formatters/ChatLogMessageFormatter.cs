﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core.Formatters
{
	/// <![CDATA[
	/// [03.03.2015 23:26:23] Some User:
	///		>> [22:59:00] Some Other User: 
	///		>> herp derp derp help
	///		>>
	///	bla-bla-bla, bla
	/// ]]>
	public sealed class ChatLogMessageFormatter : IEntityFormatter<Message>, IEntityWriter<Message>
	{
		private static readonly Regex LinkRegex = new Regex(@"<a\shref=""(.+?)"".*?>.+?</a>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex SmileRegex = new Regex(@"<ss.*?>(.+?)</ss>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex BoldRegex = new Regex(@"<b\s.*?>(.+?)</b>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex ItalicRegex = new Regex(@"<i\s.*?>(.+?)</i>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex StrikeRegex = new Regex(@"<s\s.*?>(.+?)</s>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex LegacyQuoteRegex = new Regex(@"<legacyquote>.*?</legacyquote>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex QuoteRegex = new Regex(@"<quote\s.*?>.*?</quote>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex URIObjectRegex = new Regex(@"<URIObject.*?>.*?</URIObject>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex PartlistRegex = new Regex(@"<partlist.*?>.*?</partlist>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex FilesRegex = new Regex(@"<files.*?>.*?</files>", RegexOptions.Singleline | RegexOptions.IgnoreCase);

		private readonly IAuthorNameStrategy _authorNameStrategy;

		private Message _previousMessage;

		public ChatLogMessageFormatter(IAuthorNameStrategy authorNameStrategy)
		{
			if (authorNameStrategy == null)
			{
				throw new ArgumentNullException(nameof(authorNameStrategy));
			}

			_authorNameStrategy = authorNameStrategy;
		}

		public string Format(Message message)
		{
			if (message == null)
			{
				throw new ArgumentNullException(nameof(message));
			}

			MessageType messageType = message.MessageType.HasValue ? (MessageType)message.MessageType.Value : MessageType.Message;

			MessageFormat format;

			string result;

			switch (messageType)
			{
				case MessageType.Message:
					{
						result = ParseMessageBody(message);
						format = MessageFormat.Message;
						break;
					}
				case MessageType.Status:
					{
						result = ParseMessageBody(message);
						format = MessageFormat.Action;
						break;
					}
				case MessageType.ChangeChatInfo:
					{
						result = ParseChangeChatInfo(message);
						format = MessageFormat.Action;
						break;
					}
				case MessageType.AddChatMember:
					{
						result = ParseAddChatMember(message);
						format = MessageFormat.Action;
						break;
					}
				case MessageType.KickChatMember:
					{
						result = ParseKickChatMember(message);
						format = MessageFormat.Action;
						break;
					}
				case MessageType.LeaveChat:
					{
						result = ParseLeaveChat(message);
						format = MessageFormat.Action;
						break;
					}
				case MessageType.Call:
					{
						result = ParseCall(message);
						format = MessageFormat.Action;
						break;
					}
				case MessageType.File:
					{
						result = ParseFileTransfer(message);
						format = MessageFormat.Action;
						break;
					}
				case MessageType.Image:
				case MessageType.Clip:
					{
						result = ParseURIObject(message);
						format = MessageFormat.Action;
						break;
					}
				default:
					{
						result = null;
						format = MessageFormat.Action;
						break;
					}
			}

			if (String.IsNullOrWhiteSpace(result))
			{
				return null;
			}

			var builder = new StringBuilder();

			// combine with previous message of the same author
			bool combineWithPrevious = _previousMessage != null
				&& message.Author == _previousMessage.Author
				&& (message.Date - _previousMessage.Date) <= TimeSpan.FromMinutes(1)
				&& message.MessageType == _previousMessage.MessageType
				&& format == MessageFormat.Message;

			if (combineWithPrevious)
			{
				builder.Append(result);
			}
			else
			{
				string authorName = _authorNameStrategy.GetAuthorName(message);

				if (_previousMessage != null)
				{
					builder.AppendLine();
				}

				if (format == MessageFormat.Action)
				{
					builder.Append($"[{message.Date.ToLocalTime()}] {authorName} {result}");
				}
				else
				{
					builder
						.AppendLine($"[{message.Date.ToLocalTime()}] {authorName}:")
						.Append(result);
				}
			}

			result = builder.ToString();

			_previousMessage = message;

			return result;
		}

		private string ParseMessageBody(Message message)
		{
			string result = message.BodyXml;

			if (String.IsNullOrWhiteSpace(result))
			{
				return result;
			}

			result = result.TrimStart();

			// link
			// <a href="https://www.youtube.com/watch?v=bGYZJ48tY_k">https://www.youtube.com/watch?v=bGYZJ48tY_k</a>
			result = LinkRegex.Replace(result, "$1");

			// smile
			// <ss type="smile">:)</ss>
			result = SmileRegex.Replace(result, "$1");

			// <b raw_pre="*" raw_post="*"></b>
			result = BoldRegex.Replace(result, "*$1*");
			// <i raw_pre="_" raw_post="_"></i>
			result = ItalicRegex.Replace(result, "_$1_");
			// <s raw_pre="~" raw_post="~"></s>
			result = StrikeRegex.Replace(result, "~$1~");

			// quote
			// <quote author="igor_pikhtin" authorname="Igor Pykhtin" conversation="19:1d0a1fb6fc1b4a41a402d6816bd612e5@thread.skype" guid="xa68da5add3f76cf17f603abf962808818e0b180c39455cde928d5d0b9a7afe4f" timestamp="1422720927"><legacyquote>[19:15:27] Igor Pykhtin: </legacyquote>some text here<legacyquote>&lt;&lt;&lt; </legacyquote></quote>
			result = LegacyQuoteRegex.Replace(result, ""); // remove <legacyquote>...</legacyquote>
			result = QuoteRegex.Replace(result, QuoteMatchEvaluator);

			// xml decode
			result = WebUtility.HtmlDecode(result);

			result = result.TrimEnd();

			return result;
		}

		private string QuoteMatchEvaluator(Match match)
		{
			var quote = SerializationUtil.DeserializeFromXml<SkypeSyntax.Quote>(match.Value);

			long timestamp = Convert.ToInt64(quote.Timestamp);
			DateTime date = DateTimeUnix.From(timestamp).ToLocalTime();
			string authorName = GetQuoteAuthorName(quote.Author, quote.AuthorDisplayName);
			string text = quote.Text ?? String.Empty;

			const string quotePrefix = "\t>> ";

			text = text.Replace(Environment.NewLine, Environment.NewLine + quotePrefix);

			return quotePrefix + $"[{date}] {authorName}:" + Environment.NewLine
				+ (!String.IsNullOrWhiteSpace(text) ? (quotePrefix + text + Environment.NewLine) : String.Empty)
				+ quotePrefix.TrimEnd() + Environment.NewLine;
		}

		private string GetQuoteAuthorName(string author, string authorDisplayName)
		{
			var message = new Message
			{
				Author = author,
				AuthorDisplayName = authorDisplayName
			};

			return _authorNameStrategy.GetAuthorName(message);
		}

		private string ParseChangeChatInfo(Message message)
		{
			string result = ParseMessageBody(message);

			return result != null ? $"поменял (-а) тему чата на \"{result}\"" : "поменял (-а) аватарку чата";
		}

		private string ParseAddChatMember(Message message)
		{
			if (message.Identities == null)
			{
				return null;
			}

			var identities = message.Identities.Split(' ');
			var names = identities.Where(name => !String.IsNullOrWhiteSpace(name)).Select(name => GetQuoteAuthorName(name, name)).ToArray();

			return "добавил (-а) в чат " + String.Join(", ", names);
		}

		private string ParseKickChatMember(Message message)
		{
			if (message.Identities == null)
			{
				return null;
			}

			var identities = message.Identities.Split(' ');
			var names = identities.Where(name => !String.IsNullOrWhiteSpace(name)).Select(name => GetQuoteAuthorName(name, name)).ToArray();

			return "исключил (-а) из чата " + String.Join(", ", names);
		}

		private string ParseLeaveChat(Message message)
		{
			return "покинул (-а) чат";
		}

		private string ParseURIObject(Message message)
		{
			// picture/clip
			// <URIObject type="Picture.1" uri="https://api.asm.skype.com/v1/objects/0-neu-d2-f5463a742fa3346cb4a8e33e291fa348" url_thumbnail="https://api.asm.skype.com/v1/objects/0-neu-d2-f5463a742fa3346cb4a8e33e291fa348/views/imgt1">Для просмотра этого общего фото перейдите по ссылке: <a href="https://login.skype.com/login/sso?go=xmmfallback?pic=0-neu-d2-f5463a742fa3346cb4a8e33e291fa348">https://login.skype.com/login/sso?go=xmmfallback?pic=0-neu-d2-f5463a742fa3346cb4a8e33e291fa348</a><OriginalName v="IMG_2174.JPG"/><meta type="photo" originalName="IMG_2174.JPG"/></URIObject>
			return message.BodyXml != null ? URIObjectRegex.Replace(message.BodyXml, URIObjectMatchEvaluator) : null;
		}

		private string URIObjectMatchEvaluator(Match match)
		{
			var uriObject = SerializationUtil.DeserializeFromXml<SkypeSyntax.URIObject>(match.Value);

			return uriObject.Uri;
		}

		private string ParseFileTransfer(Message message)
		{
			// file
			// <files alt="">
			// <file status="canceled" size="5833816" index="0" tid="4107485592">kak_obmanyvayut_pri_pokupke_avtomobilya_rukovodstvo_dlya_ekonomnykh_3643171.pdf</file></files>
			string result = message.BodyXml != null ? FilesRegex.Replace(message.BodyXml, FileMatchEvaluator) : null;

			return result != null ? WebUtility.HtmlDecode(result) : null;
		}

		private string FileMatchEvaluator(Match match)
		{
			var files = SerializationUtil.DeserializeFromXml<SkypeSyntax.FileList>(match.Value);

			return files.Alt ?? String.Empty;
		}

		private string ParseCall(Message message)
		{
			// group call
			/*
			<partlist type="started" alt="">
			  <part identity="igor_pikhtin">
				<name>Crypto</name>
				<duration>35</duration>
			  </part>
			  <part identity="star1ite">
				<name>Alex</name>
				<duration>35</duration>
			  </part>
			</partlist>
			*/
			return message.BodyXml != null ? PartlistRegex.Replace(message.BodyXml, PartlistMatchEvaluator) : null;
		}

		private string PartlistMatchEvaluator(Match match)
		{
			var parts = SerializationUtil.DeserializeFromXml<SkypeSyntax.PartList>(match.Value);

			return "начал (-а) звонок"; // + String.Join(", ", parts.Parts.Select(part => GetQuoteAuthorName(part.Identity, part.DisplayName)));
		}

		private int _count;

		void IEntityWriter<Message>.Begin(TextWriter writer)
		{
			_count = 0;
		}

		void IEntityWriter<Message>.Write(Message entity, TextWriter writer)
		{
			string text = Format(entity);

			if (!String.IsNullOrEmpty(text))
			{
				writer.WriteLine(text);

				_count++;
			}
		}

		void IEntityWriter<Message>.End(TextWriter writer)
		{
			writer.WriteLine();
			writer.WriteLine("---------------------------------------------------------------------");
			writer.WriteLine($"Всего сообщений: {_count}");
		}
	}

	public enum MessageFormat
	{
		Message,
		Action
	}
}
