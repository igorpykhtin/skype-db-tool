﻿namespace SkypeDBTool.Core.Formatters
{
	public interface IEntityFormatter<T>
	{
		string Format(T entity);
	}
}
