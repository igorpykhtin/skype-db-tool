﻿using System.IO;

namespace SkypeDBTool.Core.Formatters
{
	public interface IEntityWriter<T>
	{
		void Begin(TextWriter writer);
		void Write(T entity, TextWriter writer);
		void End(TextWriter writer);
	}
}
