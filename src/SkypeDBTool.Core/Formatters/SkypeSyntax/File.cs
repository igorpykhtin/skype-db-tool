﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SkypeDBTool.Core.Formatters.SkypeSyntax
{
	// <files alt="sent file &quot;IMG_04092013_135700.png&quot;"><file size = "131795" index="0" tid="6320840">IMG_04092013_135700.png</file></files>
	// <files alt=""><file size="600879" index="0">IMG_0410.JPG</file><file size="635510" index="1">IMG_0409.JPG</file></files>

	[XmlType("file")]
	public class File
	{
		[XmlAttribute("size")]
		public long Size { get; set; }

		[XmlAttribute("index")]
		public int Index { get; set; }

		[XmlAttribute("tid")]
		public long TId{ get; set; }

		[XmlText]
		public string Name { get; set; }
	}

	[XmlType("files")]
	public class FileList
	{
		[XmlAttribute("alt")]
		public string Alt { get; set; }

		[XmlElement("file")]
		public List<File> Files { get; set; } = new List<File>();
	}
}
