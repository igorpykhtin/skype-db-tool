﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SkypeDBTool.Core.Formatters.SkypeSyntax
{
	/*
	<partlist alt="">
  <part identity="igor_pikhtin">
    <name>Crypto</name>
    <duration>1</duration>
  </part>
  <part identity="star1ite">
    <name>Alex</name>
    <duration>1</duration>
  </part>
</partlist>
	*/
	[XmlType("part")]
	public class Part
	{
		[XmlAttribute("identity")]
		public string Identity { get; set; }

		[XmlElement("name")]
		public string DisplayName { get; set; }
	}

	[XmlType("partlist")]
	public class PartList
	{
		[XmlElement("part")]
		public List<Part> Parts { get; set; } = new List<Part>();
	}
}
