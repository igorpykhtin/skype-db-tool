﻿using System.Xml.Serialization;

namespace SkypeDBTool.Core.Formatters.SkypeSyntax
{
	// <quote author="igor_pikhtin" authorname="Igor Pykhtin" conversation="19:1d0a1fb6fc1b4a41a402d6816bd612e5@thread.skype" guid="xa68da5add3f76cf17f603abf962808818e0b180c39455cde928d5d0b9a7afe4f" timestamp="1422720927"><legacyquote>[19:15:27] Igor Pykhtin: </legacyquote>some text here<legacyquote>&lt;&lt;&lt; </legacyquote></quote>
	[XmlType("quote")]
	public class Quote
	{
		[XmlAttribute("author")]
		public string Author { get; set; }

		[XmlAttribute("authorname")]
		public string AuthorDisplayName { get; set; }

		[XmlAttribute("conversation")]
		public string ConversationIdentity { get; set; }

		[XmlAttribute("guid")]
		public string Guid { get; set; }

		[XmlAttribute("timestamp")]
		public string Timestamp { get; set; }

		[XmlText]
		public string Text { get; set; }
	}
}
