﻿using System.Xml.Serialization;

namespace SkypeDBTool.Core.Formatters.SkypeSyntax
{
	// <URIObject type="Picture.1" uri="https://api.asm.skype.com/v1/objects/0-neu-d2-f5463a742fa3346cb4a8e33e291fa348" url_thumbnail="https://api.asm.skype.com/v1/objects/0-neu-d2-f5463a742fa3346cb4a8e33e291fa348/views/imgt1">Для просмотра этого общего фото перейдите по ссылке: <a href="https://login.skype.com/login/sso?go=xmmfallback?pic=0-neu-d2-f5463a742fa3346cb4a8e33e291fa348">https://login.skype.com/login/sso?go=xmmfallback?pic=0-neu-d2-f5463a742fa3346cb4a8e33e291fa348</a><OriginalName v="IMG_2174.JPG"/><meta type="photo" originalName="IMG_2174.JPG"/></URIObject>
	[XmlType("URIObject")]
	public class URIObject
	{
		[XmlAttribute("content-type")]
		public string ContentType { get; set; }

		[XmlAttribute("uri")]
		public string Uri { get; set; }
	}
}
