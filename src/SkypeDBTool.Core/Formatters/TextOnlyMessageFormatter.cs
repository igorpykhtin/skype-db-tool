﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core.Formatters
{
	public sealed class TextOnlyMessageFormatter : IEntityFormatter<Message>, IEntityWriter<Message>
	{
		private static readonly Regex LinkRegex = new Regex(@"<a\shref=.+?</a>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex SmileRegex = new Regex(@"<ss.*?>.*?</ss>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex BoldRegex = new Regex(@"<b\s.*?>(.+?)</b>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex ItalicRegex = new Regex(@"<i\s.*?>(.+?)</i>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex StrikeRegex = new Regex(@"<s\s.*?>(.+?)</s>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex LegacyQuoteRegex = new Regex(@"<legacyquote>.*?</legacyquote>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex QuoteRegex = new Regex(@"<quote\s.*?>(.*?)</quote>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex URIObjectRegex = new Regex(@"<URIObject.*?>.*?</URIObject>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex PartlistRegex = new Regex(@"<partlist.*?>.*?</partlist>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
		private static readonly Regex FilesRegex = new Regex(@"<files.*?>.*?</files>", RegexOptions.Singleline | RegexOptions.IgnoreCase);

		public string Format(Message message)
		{
			if (message == null)
			{
				throw new ArgumentNullException(nameof(message));
			}

			MessageType messageType = message.MessageType.HasValue ? (MessageType)message.MessageType.Value : MessageType.Message;
			if (messageType != MessageType.Message
				&& messageType != MessageType.Status)
			{
				return null;
			}

			string result = message.BodyXml;

			if (String.IsNullOrWhiteSpace(result))
			{
				return result;
			}

			// link
			// <a href="https://www.youtube.com/watch?v=bGYZJ48tY_k">https://www.youtube.com/watch?v=bGYZJ48tY_k</a>
			result = LinkRegex.Replace(result, "");

			// smile
			// <ss type="smile">:)</ss>
			result = SmileRegex.Replace(result, "");

			// <b raw_pre="*" raw_post="*"></b>
			result = BoldRegex.Replace(result, "$1");
			// <i raw_pre="_" raw_post="_"></i>
			result = ItalicRegex.Replace(result, "$1");
			// <s raw_pre="~" raw_post="~"></s>
			result = StrikeRegex.Replace(result, "$1");

			// quote
			// <quote author="igor_pikhtin" authorname="Igor Pykhtin" conversation="19:1d0a1fb6fc1b4a41a402d6816bd612e5@thread.skype" guid="xa68da5add3f76cf17f603abf962808818e0b180c39455cde928d5d0b9a7afe4f" timestamp="1422720927"><legacyquote>[19:15:27] Igor Pykhtin: </legacyquote>some text here<legacyquote>&lt;&lt;&lt; </legacyquote></quote>
			result = LegacyQuoteRegex.Replace(result, ""); // remove <legacyquote>...</legacyquote>
			result = QuoteRegex.Replace(result, "$1" + Environment.NewLine);

			// picture
			// <URIObject type="Picture.1" uri="https://api.asm.skype.com/v1/objects/0-neu-d2-f5463a742fa3346cb4a8e33e291fa348" url_thumbnail="https://api.asm.skype.com/v1/objects/0-neu-d2-f5463a742fa3346cb4a8e33e291fa348/views/imgt1">Для просмотра этого общего фото перейдите по ссылке: <a href="https://login.skype.com/login/sso?go=xmmfallback?pic=0-neu-d2-f5463a742fa3346cb4a8e33e291fa348">https://login.skype.com/login/sso?go=xmmfallback?pic=0-neu-d2-f5463a742fa3346cb4a8e33e291fa348</a><OriginalName v="IMG_2174.JPG"/><meta type="photo" originalName="IMG_2174.JPG"/></URIObject>
			result = URIObjectRegex.Replace(result, "");

			// file
			// <files alt="">
			// <file status="canceled" size="5833816" index="0" tid="4107485592">kak_obmanyvayut_pri_pokupke_avtomobilya_rukovodstvo_dlya_ekonomnykh_3643171.pdf</file></files>
			result = FilesRegex.Replace(result, "");

			// group call
			/*
			<partlist type="started" alt="">
			  <part identity="igor_pikhtin">
				<name>Crypto</name>
				<duration>35</duration>
			  </part>
			  <part identity="star1ite">
				<name>Alex</name>
				<duration>35</duration>
			  </part>
			</partlist>
			*/
			result = PartlistRegex.Replace(result, "");

			// xml decode
			result = WebUtility.HtmlDecode(result);

			result = result.Trim();

			return result;
		}

		void IEntityWriter<Message>.Begin(TextWriter writer)
		{
		}

		void IEntityWriter<Message>.Write(Message entity, TextWriter writer)
		{
			string text = Format(entity);

			if (!String.IsNullOrEmpty(text))
			{
				writer.WriteLine(text);
			}
		}

		void IEntityWriter<Message>.End(TextWriter writer)
		{
		}
	}
}
