﻿using System.IO;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core.Formatters
{
	public sealed class XmlMessageFormatter : IEntityFormatter<Message>, IEntityWriter<Message>
	{
		public XmlMessageFormatter()
		{
		}

		public string Format(Message message)
		{
			return SerializationUtil.SerializeToXml(message, omitXmlDeclaration: true, omitXmlNamespaces: true);
		}

		void IEntityWriter<Message>.Begin(TextWriter writer)
		{
			// TODO: use writer.Encoding here
			writer.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			writer.WriteLine("<messages>");
		}

		void IEntityWriter<Message>.Write(Message entity, TextWriter writer)
		{
			SerializationUtil.SerializeToXml(entity, writer, omitXmlDeclaration: true, omitXmlNamespaces: true);
			writer.WriteLine();
		}

		void IEntityWriter<Message>.End(TextWriter writer)
		{
			writer.WriteLine("</messages>");
		}
	}
}
