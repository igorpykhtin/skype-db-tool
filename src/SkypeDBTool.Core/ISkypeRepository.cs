﻿using System.Collections.Generic;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core
{
	public interface ISkypeRepository
	{
		string GetActualAuthorName(string identity);
		Conversation FindConversation(string identity);
		ConversationInfo GetConversation(int conversationId);
		IEnumerable<Conversation> GetConversations();
		IEnumerable<Message> GetLastMessages(int conversationId, int limit, DateTimeInterval dateFilter);
		IEnumerable<Message> GetMessages(int conversationId, DateTimeInterval dateFilter);
		IEnumerable<Conversation> GetRecentConversations(int limit);
	}
}