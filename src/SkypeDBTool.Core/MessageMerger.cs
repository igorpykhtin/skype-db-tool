﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core
{
	public class MessageMerger
	{
		public MessageMerger()
		{
		}

		public IEnumerable<Message> Merge(params IEnumerable<Message>[] sources)
		{
			if (sources == null || sources.Length == 0)
			{
				throw new ArgumentNullException(nameof(sources));
			}

			// merge by guid
			var processedGuids = new HashSet<byte[]>(new GuidEqualityComparer());

			int mergedCount = 0;

			var enumerators = sources.Select(o => o.GetEnumerator()).ToArray();
			var messages = enumerators.Select(enumerator => enumerator.MoveNext() ? enumerator.Current : null).ToArray();

			while (messages.Any(message => message != null))
			{
				// get message with minimum Date
				var minDate = messages.Where(m => m != null).Min(m => m.Date);
				int index = messages.Select((m, i) => new { Message = m, Index = i }).Where(o => o.Message != null && o.Message.Date == minDate).Select(o => o.Index).First();
				var message = messages[index];

				bool isNew = processedGuids.Add(message.Guid);
				if (isNew)
				{
					yield return message;
				}
				else
				{
					mergedCount++;
				}

				var enumerator = enumerators[index];
				messages[index] = enumerator.MoveNext() ? enumerator.Current : null;
			}

			Debug.WriteLine($"Merged: {mergedCount}");
		}

		private class GuidEqualityComparer : IEqualityComparer<byte[]>
		{
			public bool Equals(byte[] x, byte[] y)
			{
				return x.SequenceEqual(y);
			}

			public int GetHashCode(byte[] obj)
			{
				return obj.Sum(o => o);
			}
		}
	}
}
