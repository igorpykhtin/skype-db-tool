﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core
{
	public static class MessageXmlSource
	{
		public static IEnumerable<Message> GetMessages(TextReader reader)
		{
			using (var xmlReader = XmlReader.Create(reader))
			{
				xmlReader.ReadStartElement("messages");
				while (xmlReader.IsStartElement("message"))
				{
					var message = SerializationUtil.DeserializeFromXml<Message>(xmlReader);
					
					yield return message;
				}
				xmlReader.ReadEndElement();
			}
		}

		public static IEnumerable<Message> GetMessages(TextReader reader, DateTimeInterval dateFilter)
		{
			var messages = GetMessages(reader);

			if (dateFilter.From != null)
			{
				var date = dateFilter.From.Value.ToUniversalTime();
				messages = messages.Where(m => m.Date >= date);
			}

			if (dateFilter.To != null)
			{
				var date = dateFilter.To.Value.ToUniversalTime();
				messages = messages.Where(m => m.Date < date);
			}

			return messages;
		}
	}
}
