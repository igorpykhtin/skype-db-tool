﻿using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace SkypeDBTool.Core
{
	public static class SerializationUtil
	{
		private static readonly Regex InvalidCharRegex = new Regex("[\x00-\x08\x0b\x0c\x0e-\x1f]", RegexOptions.Singleline | RegexOptions.Compiled);

		public static T DeserializeFromXml<T>(string xml)
		{
			T result;
			var serializer = new XmlSerializer(typeof(T));
			using (var reader = new StringReader(xml))
			{
				result = (T)serializer.Deserialize(reader);
			}
			return result;
		}

		public static T DeserializeFromXml<T>(XmlReader reader)
		{
			T result;
			var serializer = new XmlSerializer(typeof(T));
			result = (T)serializer.Deserialize(reader);
			return result;
		}

		public static string SerializeToXml(object obj, bool omitXmlDeclaration = false, bool omitXmlNamespaces = false)
		{
			string result;
			using (var writer = new UTF8StringWriter())
			{
				SerializeToXml(obj, writer, omitXmlDeclaration: omitXmlDeclaration, omitXmlNamespaces: omitXmlNamespaces);
				result = writer.ToString();
			}
			return result;
		}

		public static void SerializeToXml(object obj, TextWriter writer, bool omitXmlDeclaration = false, bool omitXmlNamespaces = false)
		{
			var serializer = new XmlSerializer(obj.GetType());
			var xmlSettings = new XmlWriterSettings
			{
				Encoding = Encoding.UTF8,
				Indent = true,
				IndentChars = "\t",
				OmitXmlDeclaration = omitXmlDeclaration
			};

			var ns = new XmlSerializerNamespaces();
			if (omitXmlNamespaces)
			{
				//Add an empty namespace and empty value
				ns.Add("", "");
			}
			using (var xmlWriter = XmlWriter.Create(writer, xmlSettings))
			{
				serializer.Serialize(xmlWriter, obj, ns);
			}
		}

		public static string EscapeInvalidXmlChar(string xml)
		{
			return InvalidCharRegex.Replace(xml, match => "&#" + ((int)match.Value[0]).ToString("X4") + ";");
		}

		private class UTF8StringWriter : StringWriter
		{
			public override Encoding Encoding => Encoding.UTF8;
		}
	}
}
