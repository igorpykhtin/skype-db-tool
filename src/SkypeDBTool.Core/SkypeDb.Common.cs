﻿using System;
using System.Data.Common;
using System.Data.SQLite;
using System.Data.SQLite.EF6;
using System.IO;

namespace SkypeDBTool.Core
{
	partial class SkypeDb
	{
		private const string DefaultDbFileName = "main.db";

		private readonly string _connectionString;
		private readonly SQLiteProviderFactory _dbFactory;

		public SkypeDb(string path)
		{
			if (String.IsNullOrEmpty(path))
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (!path.EndsWith(".db", StringComparison.CurrentCultureIgnoreCase))
			{
				path = Path.Combine(path, DefaultDbFileName);
			}

			_connectionString = $"Data Source={path};Read Only=True;datetimeformat=UnixEpoch;datetimekind=Utc";
			_dbFactory = (SQLiteProviderFactory)DbProviderFactories.GetFactory("System.Data.SQLite.EF6");

			using (var context = CreateDbContext())
			{
				if (!context.Database.Exists())
				{
					throw new ArgumentException($"Specified Skype database file '{path}' not found", nameof(path));
				}
			}
		}

		private SQLiteConnection CreateConnection()
		{
			var connection = (SQLiteConnection)_dbFactory.CreateConnection();
			connection.ConnectionString = _connectionString;

			return connection;
		}

		private SkypeDbContext CreateDbContext()
		{
			var connection = CreateConnection();
			var context = new SkypeDbContext(connection, contextOwnsConnection: true);
			ConfigureDbContext(context);

			return context;
		}

		private SkypeDbContext CreateDbContext(DbConnection connection)
		{
			var context = new SkypeDbContext(connection, contextOwnsConnection: false);
			ConfigureDbContext(context);

			return context;
		}

		private void ConfigureDbContext(SkypeDbContext context)
		{
			context.Configuration.AutoDetectChangesEnabled = false;
		}
	}
}
