﻿using System;
using System.Collections.Generic;
using System.Linq;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core
{
	public partial class SkypeDb : ISkypeRepository
	{
		public IEnumerable<Conversation> GetConversations()
		{
			using (var context = CreateDbContext())
			{
				var result = context.Conversations.OrderBy(o => o.Id);
				foreach (var conversation in result)
				{
					yield return conversation;
				}
			}
		}

		public IEnumerable<Conversation> GetRecentConversations(int limit)
		{
			using (var context = CreateDbContext())
			{
				var result = context.Conversations.OrderByDescending(o => o.LastMessageId).Take(limit);
				foreach (var conversation in result)
				{
					yield return conversation;
				}
			}
		}

		public Conversation FindConversation(string identity)
		{
			using (var context = CreateDbContext())
			{
				return context.Conversations.Where(o => o.Identity == identity).FirstOrDefault();
			}
		}

		public ConversationInfo GetConversation(int conversationId)
		{
			using (var context = CreateDbContext())
			{
				var conversation = context.Conversations.Where(o => o.Id == conversationId).FirstOrDefault();

				if (conversation == null)
				{
					return default(ConversationInfo);
				}

				var result = new ConversationInfo
				{
					Conversation = conversation,
					NumberOfMessages = context.Messages.Count(o => o.ConversationId == conversationId)
				};

				result.Authors.AddRange(
					context.GetAuthors(conversationId)
					);

				return result;
			}
		}

		public IEnumerable<Message> GetMessages(int conversationId, DateTimeInterval dateFilter)
		{
			using (var context = CreateDbContext())
			{
				IQueryable<Message> result = context.Messages
					.Where(o => o.ConversationId == conversationId);

				if (dateFilter.From != null)
				{
					var date = dateFilter.From.Value.ToUniversalTime();
					result = result.Where(m => m.Date >= date);
				}

				if (dateFilter.To != null)
				{
					var date = dateFilter.To.Value.ToUniversalTime();
					result = result.Where(m => m.Date < date);
				}

				result = result
					.OrderBy(o => o.Id);

				foreach (var message in result)
				{
					yield return message;
				}
			}
		}

		public IEnumerable<Message> GetLastMessages(int conversationId, int limit, DateTimeInterval dateFilter)
		{
			using (var context = CreateDbContext())
			{
				IQueryable<Message> result = context.Messages
					.Where(o => o.ConversationId == conversationId);

				if (dateFilter.From != null)
				{
					var date = dateFilter.From.Value.ToUniversalTime();
					result = result.Where(m => m.Date >= date);
				}

				if (dateFilter.To != null)
				{
					var date = dateFilter.To.Value.ToUniversalTime();
					result = result.Where(m => m.Date < date);
				}

				result = result.OrderByDescending(o => o.Id)
					.Take(limit)
					.OrderBy(o => o.Id);

				foreach (var message in result)
				{
					yield return message;
				}
			}
		}

		public string GetActualAuthorName(string identity)
		{
			using (var context = CreateDbContext())
			{
				var lastMessage = context.Messages.Where(o => o.Author == identity).OrderByDescending(o => o.Id).Take(1);

				return lastMessage.FirstOrDefault()?.AuthorDisplayName;
			}
		}
	}
}
