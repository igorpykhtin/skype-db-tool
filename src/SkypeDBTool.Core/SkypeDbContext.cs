﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Core
{
	internal class SkypeDbContext : DbContext
	{
		public SkypeDbContext(DbConnection connection, bool contextOwnsConnection = true)
			: base(connection, contextOwnsConnection)
		{
		}

		public DbSet<Conversation> Conversations { get; set; }

		public DbSet<Message> Messages { get; set; }

		public IEnumerable<Author> GetAuthors(int conversationId)
		{
			var lookup = Database.SqlQuery<AuthorPair>(Properties.Scripts.GetAuthors, conversationId)
				.ToLookup(o => o.Identity);
			return lookup
				.Select(o =>
				{
					var result = new Author { Identity = o.Key };
					result.DisplayNames.AddRange(lookup[o.Key].Select(t => t.DisplayName).OrderBy(s => s));
					return result;
				});
		}
	}
}