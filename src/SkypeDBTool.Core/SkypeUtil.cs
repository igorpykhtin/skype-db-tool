﻿using System;
using System.Linq;

namespace SkypeDBTool.Core
{
	public static class SkypeUtil
	{
		public static string GuidToString(byte[] guid)
		{
			return BitConverter.ToString(guid).Replace("-", "");
		}

		public static byte[] StringToGuid(string guid)
		{
			return Enumerable.Range(0, guid.Length)
				.Where(x => x % 2 == 0)
				.Select(x => Convert.ToByte(guid.Substring(x, 2), 16))
				.ToArray();
		}
	}
}
