﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Tests
{
	static class AssertExtensions
	{
		public static void AssertAreEqual(this Message actual, Message expected)
		{
			Assert.AreEqual(expected.Id, actual.Id);
			Assert.AreEqual(expected.ConversationId, actual.ConversationId);
			Assert.AreEqual(expected.Date, actual.Date);
			Assert.IsTrue(actual.Guid.SequenceEqual(expected.Guid));
			Assert.AreEqual(expected.Author, actual.Author);
			Assert.AreEqual(expected.AuthorDisplayName, actual.AuthorDisplayName);
			Assert.AreEqual(expected.BodyXml, actual.BodyXml);
		}
	}
}
