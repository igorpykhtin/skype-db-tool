﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using SkypeDBTool.Core;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Tests
{
	[TestClass]
	public class AuthorNameStrategyTests
	{
		[TestMethod]
		public void TestDefaultAuthoNameStrategy_ShouldUseAuthorDisplayName()
		{
			var message = new Message
			{
				Author = "Some.User",
				AuthorDisplayName = "Name Surename"
			};

			var strategy = new DefaultAuthorNameStrategy();
			var result = strategy.GetAuthorName(message);

			Assert.AreEqual("Name Surename", result);
		}

		[TestMethod]
		public void TestActualAuthoNameStrategy_ShouldUseActualAuthorDisplayName()
		{
			var message = new Message
			{
				Author = "Some.User",
				AuthorDisplayName = "Name Surename"
			};

			var skype = Substitute.For<ISkypeRepository>();
			skype.GetActualAuthorName("Some.User").Returns("Actual Name Surename");

			var strategy = new ActualAuthorNameStrategy(skype);
			var result = strategy.GetAuthorName(message);

			Assert.AreEqual("Actual Name Surename", result);
		}

		[TestMethod]
		public void TestAliasAuthoNameStrategy_ShouldUseAliasWhenDefined()
		{
			var message = new Message
			{
				Author = "Some.User",
				AuthorDisplayName = "Name Surename"
			};

			var lines = new List<string>
			{
				"UserA|User A",
				"Some.User|Some User Name",
				"UserB|User B"
			};

			var strategy = new AliasAuthorNameStrategy(lines);
			var result = strategy.GetAuthorName(message);

			Assert.AreEqual("Some User Name", result);
		}

		[TestMethod]
		public void TestAliasAuthoNameStrategy_ShouldUseAuthorDisplayNameWhenAliasNotDefined()
		{
			var message = new Message
			{
				Author = "Some.User",
				AuthorDisplayName = "Name Surename"
			};

			var lines = new List<string>
			{
				"UserA|User A",
				"UserB|User B"
			};

			var strategy = new AliasAuthorNameStrategy(lines);
			var result = strategy.GetAuthorName(message);

			Assert.AreEqual("Name Surename", result);
		}
	}
}
