﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using SkypeDBTool.Core;
using SkypeDBTool.Core.Entity;
using SkypeDBTool.Core.Formatters;

namespace SkypeDBTool.Tests.Formatters
{
	[TestClass]
	public class ChatLogMessageFormatterTest
	{
		private const string MessageNewLine = "\n"; // for some reasons new line in multiline message after deserialization from CData is \n, not \r\n

		private IEntityFormatter<Message> CreateFormatter()
		{
			var authorNameStrategy = Substitute.For<IAuthorNameStrategy>();
			authorNameStrategy.GetAuthorName(Arg.Any<Message>()).Returns(args =>
			{
				var m = (Message)args[0];
				return m.AuthorDisplayName;
			});

			return new ChatLogMessageFormatter(authorNameStrategy);
		}

		private void CheckChatLogFormat(string expectedText, Message message, string actualResult, MessageFormat format)
		{
			string expectedResult = format == MessageFormat.Action
				? $"[{message.Date.ToLocalTime()}] {message.AuthorDisplayName} {expectedText}"
				: $"[{message.Date.ToLocalTime()}] {message.AuthorDisplayName}:" + Environment.NewLine + expectedText;

			Debug.WriteLine(actualResult);

			Assert.AreEqual(expectedResult, actualResult);
        }

		public void TestChatLogMessageFormatter_Format(string expectedText, string messageXml, MessageFormat format = MessageFormat.Message)
		{
			var message = SerializationUtil.DeserializeFromXml<Message>(messageXml);

			var formatter = CreateFormatter();
			var result = formatter.Format(message);

			CheckChatLogFormat(expectedText, message, result, format);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Empty()
		{
			var message = SerializationUtil.DeserializeFromXml<Message>(Properties.MessageSamples.Empty);

			var formatter = CreateFormatter();
			var result = formatter.Format(message);

			Assert.IsNull(result);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Simple()
		{
			string expectedText =
"простое сообщение";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.Simple);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_SimpleMultiline()
		{
			string expectedText =
"многострочное сообщение" + MessageNewLine +
"вторая строка";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.SimpleMultiline);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Link()
		{
			string expectedText =
"пример ссылки http://www.youtube.com/watch?v=re1EatGRV0w" + MessageNewLine +
"с описанием";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.Link);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Smile()
		{
			string expectedText =
"сообщение (facepalm) со смайлами :)";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.Smile);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_BoldItalicStrike()
		{
			string expectedText =
"сообщение *выделенный текст*, _курсив_ и ~перечеркнутый~";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.BoldItalicStrike);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Quote()
		{
			string expectedText =
"\t>> [14.02.2016 15:28:53] Some Author:" + Environment.NewLine +
"\t>> текст цитаты" + Environment.NewLine +
"\t>>" + Environment.NewLine +
"ответ на цитату";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.Quote);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Quote2()
		{
			string expectedText =
"\t>> [14.02.2016 15:28:53] Some Author:" + Environment.NewLine +
"\t>> текст цитаты" + Environment.NewLine +
"\t>>" + Environment.NewLine +
"ответ на цитату";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.Quote2);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_QuoteEmpty()
		{
			string expectedText =
"\t>> [14.02.2016 15:28:53] Some Author:" + Environment.NewLine +
"\t>>" + Environment.NewLine +
"ответ на цитату";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.QuoteEmpty);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Encoded()
		{
			string expectedText =
"пример тэга <script>alert('asd')</script> в сообщении";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.Encoded);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_URIObject()
		{
			string expectedText =
"https://api.asm.skype.com/v1//objects/0-weu-d1-xxxx";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.URIObject, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Files()
		{
			string expectedText =
"отправил (-а) файлы \"IMG_0410.JPG\", \"IMG_0409.JPG\"";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.Files, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_FilesAlt()
		{
			string expectedText =
"отправил (-а) файлы \"IMG_04092013_135700.png\"";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.FilesAlt, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_GroupCall()
		{
			string expectedText =
"начал (-а) звонок";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.GroupCall, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_Status()
		{
			string expectedText =
"совершил действие";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.Status, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_ChangeChatDisplayName()
		{
			string expectedText =
"поменял (-а) тему чата на \"новое название чата\"";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.ChatDisplayName, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_ChangeChatAvatar()
		{
			string expectedText =
"поменял (-а) аватарку чата";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.ChatAvatar, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_AddChatMember()
		{
			string expectedText =
"добавил (-а) в чат some.user, another.user";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.AddChatMember, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_KickChatMember()
		{
			string expectedText =
"исключил (-а) из чата some.user, another.user";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.KickChatMember, MessageFormat.Action);
		}

		[TestMethod]
		public void TestChatLogMessageFormatter_Format_LeaveChat()
		{
			string expectedText =
"покинул (-а) чат";
			TestChatLogMessageFormatter_Format(expectedText, Properties.MessageSamples.LeaveChat, MessageFormat.Action);
		}
	}
}
