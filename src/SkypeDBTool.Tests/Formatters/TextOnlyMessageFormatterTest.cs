﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkypeDBTool.Core;
using SkypeDBTool.Core.Entity;
using SkypeDBTool.Core.Formatters;

namespace SkypeDBTool.Tests.Formatters
{
	[TestClass]
	public class TextOnlyMessageFormatterTest
	{
		private const string MessageNewLine = "\n"; // for some reasons new line in multiline message after deserialization from CData is \n, not \r\n

		private IEntityFormatter<Message> CreateFormatter()
		{
			return new TextOnlyMessageFormatter();
		}

		public void TestTextOnlyMessageFormatter_Format(string expectedText, string messageXml)
		{
			var message = SerializationUtil.DeserializeFromXml<Message>(messageXml);

			var formatter = CreateFormatter();
			var result = formatter.Format(message);

			Debug.WriteLine(result);

			Assert.AreEqual(expectedText, result);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Empty()
		{
			var message = SerializationUtil.DeserializeFromXml<Message>(Properties.MessageSamples.Empty);

			var formatter = CreateFormatter();
			var result = formatter.Format(message);

			Assert.IsNull(result);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Simple()
		{
			string expectedText =
"простое сообщение";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.Simple);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_SimpleMultiline()
		{
			string expectedText =
"многострочное сообщение" + MessageNewLine +
"вторая строка";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.SimpleMultiline);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Link()
		{
			string expectedText =
"пример ссылки " + MessageNewLine +
"с описанием";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.Link);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Smile()
		{
			string expectedText =
"сообщение  со смайлами";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.Smile);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_BoldItalicStrike()
		{
			string expectedText =
"сообщение выделенный текст, курсив и перечеркнутый";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.BoldItalicStrike);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Quote()
		{
			string expectedText =
"текст цитаты" + Environment.NewLine +
"ответ на цитату";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.Quote);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Quote2()
		{
			string expectedText =
"текст цитаты" + Environment.NewLine +
"ответ на цитату";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.Quote2);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_QuoteEmpty()
		{
			string expectedText =
"ответ на цитату";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.QuoteEmpty);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Encoded()
		{
			string expectedText =
"пример тэга <script>alert('asd')</script> в сообщении";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.Encoded);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_URIObject()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.URIObject);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Files()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.Files);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_FilesAlt()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.FilesAlt);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_GroupCall()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.GroupCall);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_Status()
		{
			string expectedText =
"совершил действие";
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.Status);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_ChangeChatDisplayName()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.ChatDisplayName);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_ChangeChatAvatar()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.ChatAvatar);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_AddChatMember()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.AddChatMember);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_KickChatMember()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.KickChatMember);
		}

		[TestMethod]
		public void TestTextOnlyMessageFormatter_Format_LeaveChat()
		{
			string expectedText = null;
			TestTextOnlyMessageFormatter_Format(expectedText, Properties.MessageSamples.LeaveChat);
		}
	}
}
