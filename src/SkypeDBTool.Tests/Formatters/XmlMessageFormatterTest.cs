﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkypeDBTool.Core;
using SkypeDBTool.Core.Entity;
using SkypeDBTool.Core.Formatters;

namespace SkypeDBTool.Tests.Formatters
{
	[TestClass]
	public class XmlMessageFormatterTest
	{
		private void TestXmlMessageFormatter_Format(string messageXml)
		{
			var message = SerializationUtil.DeserializeFromXml<Message>(messageXml);

			var formatter = new XmlMessageFormatter();
			var result = formatter.Format(message);

			var resultMessage = SerializationUtil.DeserializeFromXml<Message>(result);
			resultMessage.AssertAreEqual(message);
		}

		[TestMethod]
		public void TestXmlMessageFormatter_Format()
		{
			TestXmlMessageFormatter_Format(Properties.MessageSamples.Simple);
		}

		[TestMethod]
		public void TestXmlMessageFormatter_Format_ShouldEscapeInvalidChar()
		{
			var message = SerializationUtil.DeserializeFromXml<Message>(Properties.MessageSamples.Simple);

			message.BodyXml = "Some \x0014 invalid text";

			var formatter = new XmlMessageFormatter();
			var result = formatter.Format(message);

			var resultMessage = SerializationUtil.DeserializeFromXml<Message>(result);
			Assert.AreEqual("Some &#0014; invalid text", resultMessage.BodyXml);
		}
	}
}
