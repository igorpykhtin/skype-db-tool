﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkypeDBTool.Core;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Tests
{
	[TestClass]
	public class MessageMergerTest
	{
		private readonly DateTime Date1 = new DateTime(2016, 2, 1);
		private readonly DateTime Date2 = new DateTime(2016, 2, 5);
		private readonly DateTime Date3 = new DateTime(2016, 2, 10);
		private readonly DateTime Date4 = new DateTime(2016, 2, 15);

		private byte[] SkypeGuid(byte i)
		{
			var guid = new byte[32];
			guid[0] = i;
			return guid;
		}

		[TestMethod]
		public void TestMessageMerger_Merge_WhenSingleSource()
		{
			var m1 = new Message { Date = Date1, Guid = SkypeGuid(1) };
			var m2 = new Message { Date = Date2, Guid = SkypeGuid(2) };
			var m3 = new Message { Date = Date3, Guid = SkypeGuid(3) };
			var m4 = new Message { Date = Date4, Guid = SkypeGuid(4) };

			var source = new List<Message> { m1, m2, m3, m4 };
			var expected = new List<Message> { m1, m2, m3, m4 };

			var merger = new MessageMerger();
			var result = merger.Merge(source);

			CollectionAssert.AreEqual(result.ToArray(), expected);
		}

		[TestMethod]
		public void TestMessageMerger_Merge_WhenSecondSourceIsEmpty()
		{
			var m1 = new Message { Date = Date1, Guid = SkypeGuid(1) };
			var m2 = new Message { Date = Date2, Guid = SkypeGuid(2) };
			var m3 = new Message { Date = Date3, Guid = SkypeGuid(3) };
			var m4 = new Message { Date = Date4, Guid = SkypeGuid(4) };

			var first = new List<Message> { m1, m2, m3, m4 };
			var second = new List<Message>  { };
			var expected = new List<Message> { m1, m2, m3, m4 };

			var merger = new MessageMerger();
			var result = merger.Merge(first, second);

			CollectionAssert.AreEqual(result.ToArray(), expected);
		}

		[TestMethod]
		public void TestMessageMerger_Merge_WhenFirstSourceIsEmpty()
		{
			var m1 = new Message { Date = Date1, Guid = SkypeGuid(1) };
			var m2 = new Message { Date = Date2, Guid = SkypeGuid(2) };
			var m3 = new Message { Date = Date3, Guid = SkypeGuid(3) };
			var m4 = new Message { Date = Date4, Guid = SkypeGuid(4) };

			var first = new List<Message> { };
			var second = new List<Message> { m1, m2, m3, m4 };
			var expected = new List<Message> { m1, m2, m3, m4 };

			var merger = new MessageMerger();
			var result = merger.Merge(first, second);

			CollectionAssert.AreEqual(result.ToArray(), expected);
		}

		[TestMethod]
		public void TestMessageMerger_Merge_WhenSourcesDontContainSameMessages()
		{
			var m1 = new Message { Date = Date1, Guid = SkypeGuid(1) };
			var m2 = new Message { Date = Date2, Guid = SkypeGuid(2) };
			var m3 = new Message { Date = Date3, Guid = SkypeGuid(3) };
			var m4 = new Message { Date = Date4, Guid = SkypeGuid(4) };

			var first = new List<Message> { m1, m4 };
			var second = new List<Message> { m2, m3 };
			var expected = new List<Message> { m1, m2, m3, m4 };

			var merger = new MessageMerger();
			var result = merger.Merge(first, second);

			CollectionAssert.AreEqual(result.ToArray(), expected);
		}

		[TestMethod]
		public void TestMessageMerger_Merge_WhenSourcesContainSameMessages()
		{
			var m1 = new Message { Date = Date1, Guid = SkypeGuid(1) };
			var m1x = new Message { Date = Date1.AddSeconds(15), Guid = SkypeGuid(1) };
			var m2 = new Message { Date = Date2, Guid = SkypeGuid(2) };
			var m2x = new Message { Date = Date2.AddMinutes(2), Guid = SkypeGuid(2) };
			var m3 = new Message { Date = Date3, Guid = SkypeGuid(3) };
			var m3x = new Message { Date = Date3.AddSeconds(10), Guid = SkypeGuid(3) };
			var m4 = new Message { Date = Date4, Guid = SkypeGuid(4) };
			var m4x = new Message { Date = Date4.AddMinutes(5), Guid = SkypeGuid(4) };

			var first = new List<Message> { m1x, m2, m3, m4x };
			var second = new List<Message> { m2x, m4 };
			var third = new List<Message> { m1, m3x };
			var expected = new List<Message> { m1, m2, m3, m4 };

			var merger = new MessageMerger();
			var result = merger.Merge(first, second, third);

			CollectionAssert.AreEqual(result.ToArray(), expected);
		}
	}
}
