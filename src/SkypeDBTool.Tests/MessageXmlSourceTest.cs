﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkypeDBTool.Core;

namespace SkypeDBTool.Tests
{
	[TestClass]
	public class MessageXmlSourceTest
	{
		[TestMethod]
		public void TestMessageXmlSourceTest_GetdMessages()
		{
			using (var reader = new StringReader(Properties.MessageSamples.List))
			{
				var messages = MessageXmlSource.GetMessages(reader).ToList();

				Assert.AreEqual(3, messages.Count);
				Assert.AreEqual("пример простого текста", messages[0].BodyXml);
				Assert.AreEqual("еще один пример простого текста", messages[1].BodyXml);
				Assert.AreEqual("ответ", messages[2].BodyXml);
			}
		}

		[TestMethod]
		public void TestMessageXmlSourceTest_GetdMessages_ByYear()
		{
			using (var reader = new StringReader(Properties.MessageSamples.List))
			{
				var messages = MessageXmlSource.GetMessages(reader, DateTimeInterval.FromYear(2013)).ToList();

				Assert.AreEqual(2, messages.Count);
				Assert.AreEqual("еще один пример простого текста", messages[0].BodyXml);
				Assert.AreEqual("ответ", messages[1].BodyXml);
			}
		}
	}
}
