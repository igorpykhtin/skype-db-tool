﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkypeDBTool.Core;

namespace SkypeDBTool.Tests
{
	[TestClass]
	public class SerializationUtilTest
	{
		[TestMethod]
		public void TestSerializationUtil_EscapeInvalidXmlChar_ValidChar()
		{
			string result;

			result = SerializationUtil.EscapeInvalidXmlChar("one\ttwo\nthree\r\nfour");
			Assert.AreEqual("one\ttwo\nthree\r\nfour", result);
		}

		[TestMethod]
		public void TestSerializationUtil_EscapeInvalidXmlChar_InvalidChar()
		{
			string result;

			result = SerializationUtil.EscapeInvalidXmlChar("one \x00 two \x01 three \x14 four \x1f five");
			Assert.AreEqual("one &#0000; two &#0001; three &#0014; four &#001F; five", result);
		}
	}
}
