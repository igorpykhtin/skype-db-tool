﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkypeDBTool.Core;
using SkypeDBTool.Core.Entity;

namespace SkypeDBTool.Tests
{
	[TestClass]
	public class SkypeDbTest
	{
		private string SkypProfilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Skype", Properties.Settings.Default.SkypAccountName);

		private bool CheckConversation(Conversation conversation)
		{
			Assert.IsNotNull(conversation);
			Assert.IsTrue(conversation.Id > 0);
			Assert.IsNotNull(conversation.DisplayName);
			Assert.IsNotNull(conversation.Identity);
			Assert.IsTrue(conversation.CreateDate > DateTimeUnix.BaseDate);
			if (conversation.LastMessageId.HasValue)
			{
				Assert.IsTrue(conversation.LastMessageId > 0);
			}
			if (conversation.LastActivityDate.HasValue)
			{
				Assert.IsTrue(conversation.LastActivityDate.Value > DateTimeUnix.BaseDate);
			}

			return true;
		}

		private bool CheckAuthor(Author author)
		{
			Assert.IsNotNull(author);
			Assert.IsNotNull(author.Identity);
			Assert.IsNotNull(author.DisplayNames);
			Assert.IsTrue(author.DisplayNames.Any());
			Assert.IsTrue(author.DisplayNames.All(o => o != null));

			return true;
		}


		private bool CheckMessage(Message message)
		{
			Assert.IsNotNull(message);
			Assert.IsTrue(message.Id > 0);
			Assert.IsNotNull(message.Author);
			Assert.IsNotNull(message.AuthorDisplayName);
			//Assert.IsNotNull(message.BodyXml);
			Assert.IsTrue(message.Date > DateTimeUnix.BaseDate);

			return true;
		}

		[TestMethod]
		public void TestSkypeDb_GetConversations()
		{
			var skype = new SkypeDb(SkypProfilePath);
			var conversations = skype.GetConversations().Where(o => o.LastActivityDate.HasValue).ToList();

			Assert.IsNotNull(conversations);
			Assert.IsTrue(conversations.Any());

			var conversation = conversations.First();
			CheckConversation(conversation);
		}

		[TestMethod]
		public void TestSkypeDb_GetRecentConversations()
		{
			const int limit = 5;

			var skype = new SkypeDb(SkypProfilePath);
			var conversations = skype.GetRecentConversations(limit).ToList();

			Assert.IsNotNull(conversations);
			Assert.AreEqual(limit, conversations.Count);
			Assert.IsTrue(conversations.All(CheckConversation));
		}

		[TestMethod]
		public void TestSkypeDb_FindConversation()
		{
			var skype = new SkypeDb(SkypProfilePath);
			var conversation = skype.GetRecentConversations(1).First();

			var result = skype.FindConversation(conversation.Identity);

			Assert.IsNotNull(result);
			CheckConversation(result);
			Assert.AreEqual(conversation.Id, result.Id);
		}

		[TestMethod]
		public void TestSkypeDb_GetConversation()
		{
			var skype = new SkypeDb(SkypProfilePath);
			var conversationId = skype.GetRecentConversations(1).First().Id;

			var info = skype.GetConversation(conversationId);

			Assert.IsNotNull(info);
			CheckConversation(info.Conversation);
			Assert.AreEqual(conversationId, info.Conversation.Id);
			Assert.AreNotEqual(0, info.NumberOfMessages);
			Assert.IsNotNull(info.Authors);
			Assert.IsTrue(info.Authors.Any());
			Assert.IsTrue(info.Authors.All(CheckAuthor));
		}

		[TestMethod]
		public void TestSkypeDb_GetMessages()
		{
			var skype = new SkypeDb(SkypProfilePath);
			var conversationId = skype.GetRecentConversations(1).First().Id;

			var messages = skype.GetMessages(conversationId, DateTimeInterval.Infinity).Take(10).ToList();

			Assert.IsNotNull(messages);
			Assert.IsTrue(messages.Count > 0);
			Assert.IsTrue(messages.All(o => o.ConversationId == conversationId));
			Assert.IsTrue(messages.All(CheckMessage));
		}

		[TestMethod]
		public void TestSkypeDb_GetMessages_ByYear()
		{
			int year = DateTime.Today.Year;

			var skype = new SkypeDb(SkypProfilePath);
			var conversationId = skype.GetRecentConversations(1).First().Id;

			var messages = skype.GetMessages(conversationId, DateTimeInterval.FromYear(year)).Take(10).ToList();

			Assert.IsNotNull(messages);
			Assert.IsTrue(messages.Count > 0);
			Assert.IsTrue(messages.All(o => o.ConversationId == conversationId));
			Assert.IsTrue(messages.All(o => o.Date.ToLocalTime().Year == year));
			Assert.IsTrue(messages.All(CheckMessage));
		}

		[TestMethod]
		public void TestSkypeDb_GetLastMessages()
		{
			const int limit = 10;

			var skype = new SkypeDb(SkypProfilePath);
			var conversationId = skype.GetRecentConversations(1).First().Id;

			var messages = skype.GetLastMessages(conversationId, limit, DateTimeInterval.Infinity).ToList();

			Assert.IsNotNull(messages);
			Assert.IsTrue(messages.Count > 0);
			Assert.IsTrue(messages.All(o => o.ConversationId == conversationId));
			Assert.IsTrue(messages.All(CheckMessage));
		}

		[TestMethod]
		public void TestSkypeDb_GetLastMessages_ByYear()
		{
			const int limit = 10;
			int year = DateTime.Today.Year;

			var skype = new SkypeDb(SkypProfilePath);
			var conversationId = skype.GetRecentConversations(1).First().Id;

			var messages = skype.GetLastMessages(conversationId, limit, DateTimeInterval.FromYear(year)).ToList();

			Assert.IsNotNull(messages);
			Assert.IsTrue(messages.Count > 0);
			Assert.IsTrue(messages.All(o => o.ConversationId == conversationId));
			Assert.IsTrue(messages.All(o => o.Date.ToLocalTime().Year == year));
			Assert.IsTrue(messages.All(CheckMessage));
		}

		[TestMethod]
		public void TestSkypeDb_GetActualAuthorName()
		{
			var skype = new SkypeDb(SkypProfilePath);
			var conversationId = skype.GetRecentConversations(1).First().Id;
			var lastMessage = skype.GetLastMessages(conversationId, 1, DateTimeInterval.Infinity).First();

			var result = skype.GetActualAuthorName(lastMessage.Author);

			Assert.AreEqual(lastMessage.AuthorDisplayName, result);
		}
	}
}
